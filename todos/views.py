from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList

# Create your views here.


def todo_list_list(request):
    list = TodoList.objects.all()
    context = {
        "list": list
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todos = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todos,
    }
    return render(request, "todos/detail.html", context)



def create_todo(request):
    if request.method == "POST":

        form = 
